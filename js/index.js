$(function () {
  $('[data-toggle="tooltip"]').tooltip();
  $('[data-toggle="popover"]').popover();
  $('.carousel').carousel({
  	interval: 6000	
  });
  $('#contact').on('show.bs.modal',function(e) {
  	console.log('el modal se está mostrando');
  	$('#contact-btn').removeClass('btn-outline-success');
  	$('#contact-btn').addClass('btn-disabled');
  	$('#contact-btn').prop('disabled', true);
  });

  $('#contact').on('hidden.bs.modal',function(e) {
  	$('#contact-btn').removeClass('btn-disabled');
  	$('#contact-btn').addClass('btn-outline-success');
  	$('#contact-btn').prop('disabled', false);
  })
});
 